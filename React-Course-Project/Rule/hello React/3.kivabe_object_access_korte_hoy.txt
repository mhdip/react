Kivabe object access korte hoy 

1.

var user = {
    name: 'Mh Dip',
    age: 26,
    location: 'Bangladesh',
}

var templateTwo = (
    <div>
        <h1>{user.name + '!'}</h1>
        <p>{user.age}</p>
        <p>{user.location}</p>
    </div>
);

var appRoot = document.getElementById('app');
ReactDOM.render(templateTwo, appRoot)