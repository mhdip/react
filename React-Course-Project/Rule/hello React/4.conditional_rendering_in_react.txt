If statements, ternary operators, logical and operators

code 
var user = {
    name: 'MH Dip',
    age : 26,
    location: 'Bangladesh'
};
function getLocation(location){
    if(location){
        return <p>Location: {location}</p>;
    }
}
var template = (
    <div>
        <h1>{user.name}</h1>
        <p>Age: {user.age}</p>
         {getLocation(user.location)}
    </div>
);
var appRoot = document.getElementById('app');
ReactDOM.render(template, appRoot)

akhane amra jeta korechi seta holo,  jsx e function call korchi 
and function e if condition dieche.. jeta output dibe.. jodi location thake
thle return location return koro or empty rakho 


****Ternary Operator *** 

code
var user = {
    name: 'MH Dip',
    age : 26,
    //location: 'Bangladesh'
};
function getLocation(location){
    if(location){
        return <p>Location: {location}</p>;
    }
}
var template = (
    <div>
        <h1>{user.name ? user.name: 'Annonymouse'}</h1>
        <p>Age: {user.age}</p>
         {getLocation(user.location)}
    </div>
);
var appRoot = document.getElementById('app');
ReactDOM.render(template, appRoot)

akhane template jsx e ar h1 e amra jeta bolechi seta holo, 
jodi user.name thake thle user.name return koro ar nah thakele Annonymouse return koro 

user.name ? user.name: 'Annonymouse'
.           .				.
.				.					.
.					.						.
.						.							.
true(jodi thake) ? name ta return koro : ar jodi nah thake tahole Annonymouse return koro 


