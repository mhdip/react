/*
    1. only render the subtitle and p tag
    2. if subtitle exist use logical && operator
    3. if it exist show one thing otherwise nothing
    4. render a brand new paragraph tag
    5. if options.length > 0 "Here are your option" or "No options"
*/   

var app = {
    title: 'Indecistion App',
    subtitle: 'Put your life hands of a computer',
    options : ['one', 'two', 'three']
}
var templateTwo = (
    <div>
        <h1>{app.title + '!'}</h1>
        {app.subtitle && <p>{app.subtitle}</p>}
        <p>{app.options.length > 0 ? 'Here are your options' : 'no option'}</p>
        <ol>
            <li>Item One</li>
            <li>Item Two</li>
        </ol>
    </div>
);
var user = {
    name: 'MH Dip',
    age : 19,
    location: 'Bangladesh'
};
function getLocation(location){
    if(location){
        return <p>Location: {location}</p>;
    }
} 
var template = (
    <div>
        <h1>{user.name ? user.name: 'Annonymouse'}</h1>
        {(user.age && user.age > 18) && <p>Age: {user.age}</p>}
         {getLocation(user.location)}
    </div>
);
var appRoot = document.getElementById('app');
ReactDOM.render(templateTwo, appRoot)

