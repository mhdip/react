/*
  1. should contain one button 
  2. when it click show some content 
  3. when it click again hide the conte 
  4. make sure button text is changes 
*/

/*First Solution*/
// const content = {
//     title: "Visibilty Toggle",
//     context: []
// }

// const VisibilityToggle = (e)=> {
//     //content.context.push(1)
//     let conVal= content.context.length;

//     if(conVal === 0){
//         content.context.push(1)
//         conVal = 1;
//         //console.log('show');

//     }else if(conVal > 0){
//         content.context = [];
//         //console.log('hide');
//     }
//     render();
//     return; 
// }

// const appRoot = document.getElementById('app');
// const render = ()=> {
//     const template = (
//         <div>
//             <h1>{content.title}</h1>
//             <button onClick={VisibilityToggle}>{content.context.length > 0 ? 'Show Details' : 'Hide Details' }</button>
//             <p>{content.context.length > 0 ? 'This is some content' : '' }</p>
    
//         </div>
//     );
//     ReactDOM.render(template,appRoot) 
// }

// render();

/*Second SOlution*/
let visibility = false;

const  toggleVisibility = () => {
    visibility = !visibility;
    render();

}

const render = () => {
    const jsx = (
        <div>
            <h1>Visibility Toggle</h1>
            <button onClick={toggleVisibility}>
                {visibility ? 'Hide Details' : 'Show Details'}
            </button>
             {
                 visibility && (
                     <div>
                        <p>Hey, These are some details you can now see</p>
                     </div>
                 )
             }
        </div>
    )
    ReactDOM.render(jsx, document.getElementById('app')); 
}
render();


