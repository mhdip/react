//console.log('App.js is running');

//JSX - JavaScript XML
 

const userInfo = {
    name : 'Mhdip',
    age : 18,
    location: 'Bangladesh'
}

function getLocation(location){
    if(location){
        return <p>Location: {userInfo.location}</p>
    }
}

const templateTwo = (
    <div>
       {/* using ternary*/}
       <h1>{userInfo.name ? userInfo.name : 'Annonymouse'}</h1>
       {/* Condition*/}
       { (userInfo.age && userInfo.age >=18) && <p>Age: {userInfo.age}</p>}
       <p>{getLocation(userInfo.location)}</p>
    </div>
)

const myInfo = document.getElementById('myInfo');
ReactDOM.render(templateTwo, myInfo);