var nameVar = 'Andrew';
nameVar = 'Mike';

console.log('nameVar', nameVar);

let nameLet = 'Jen';
nameLet = 'Julue';
console.log('nameLet', nameLet)

const nameConst = 'Frank';
console.log('nameConst', nameConst);  

function getPetName(){
    var petName = 'SimSim';
    return petName;
}

const a = getPetName();
console.log(a)

var fullName = 'Jen Mead';
let firstName;
if(fullName){
    firstName = fullName.split(' ')[0];
    console.log(firstName);
}

console.log(firstName);

