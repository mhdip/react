/*
    challange...

    1. make method getFirstName('Mike Smith')
    2. create regular arrow function 
    3. create arrow function using shorthand syntaxt


*/ 

// const getFirstName = (name) => {
//     return name.split(' ')[0];
// }

const getFirstName = (name) => name.split(' ')[0];

console.log(getFirstName('ram mike'))

const square = function (x){
    return x * x;
}

console.log(square(8))

const squareArrow = (x)=>{
    return x * x;
}

console.log(squareArrow(9)); 

//if it return single value with single parameter

const number = (x)=> x*x;

console.log("using_single_statement: "+ number(3))

