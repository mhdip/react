//console.log('App.js is running');

//JSX - JavaScript XML
 
const app = {
    title: 'Indecision App',
    subtitle: 'Put Your life ins the hands of a computer',
    options : []
};

const onMakeDecision = () => {
    /*
        generate random number 
    */ 

    const randomNum = Math.floor(Math.random() * app.options.length);
    const option = app.options[randomNum];
    alert(option);
   //console.log(randomNum);


}

const onFormSubmit = (e)=> {
    e.preventDefault();

    const option = e.target.elements.option.value;

    //check if empty
    if(option){
        app.options.push(option);
        e.target.elements.option.value = '';
    }  
    console.log(option);  
    renderOption();
}

const RemoveAll = (e)=> {
    
    app.options= [];
    renderOption();
}

const numbers = [55, 101, 1000];

const appRoot = document.getElementById('app');
const renderOption  = ()=> {
    const template = (
        <div>
            <h1>{app.title}</h1>
            {app.subtitle && <p>{app.subtitle}</p>}
            <p>{app.options.length > 0 ?'Here are your options' : 'No Options'}</p>
            <button disabled={app.options.length===0} onClick={onMakeDecision}>What should i do? </button>
            {/*array start*/}
            {/*
                {ww will use map function
                [<p key="1">a</p>,<p key="2">b</p>,<p key="">c</p>]
                }                 
            */}

            {
                /*Render Things Dynamicly thorugh array*/
                numbers.map((numbers)=>{
                   return <p key={numbers}>Number: {numbers}</p>;
                })

            }
            <ol>
              {/*map over app.options getting back and array of list (set kye and text)*/}
                {
                    
                    // app.options.map((option)=>{
                    //      return <li key={option}>Option: {option}</li>
                    // })
                    //sort form
                    app.options.map((option)=><li key={option}>{option}</li>)
                }
            </ol>
          
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option"/>
                <button>Add Option</button>
                <button onClick={RemoveAll}>Remove All</button>
            </form>
        </div>   
    );
    ReactDOM.render(template, appRoot);
}

renderOption();

//challange
/*
    create "Remove All" button
    need onClick Handler -> wipe teh array -> rerender 
    count should go back to 0 
*/ 


