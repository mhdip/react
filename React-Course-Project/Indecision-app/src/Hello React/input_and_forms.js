console.log('App.js is running');

/*
    1. create Remove All button 
    2. made on click function-> wipe the array -> rerender

*/

const app = {
    title: 'Indecision App',
    subtitle: 'Put Your life in teh hands of a computer',
    options: []
}

const onFormSubmit = (e) => {
    e.preventDefault();
    
    const option = e.target.elements.option.value;

    if(option){
        app.options.push(option);
        e.target.elements.option.value = ''; 
        reReander();
    }
};

const RemoveAll = () => {
    app.options = [];
     reReander();
}

const appRoot = document.getElementById('app')
function reReander(){
    const template = (
        <div>
            <h1>{app.title}</h1>
            {app.subtitle && <p>{app.subtitle}</p>}
            <p>{app.options.length>0 ? 'Here are your options' : 'No options'}</p>
            <p>
                {app.options.length }         
            </p>  
            <button onClick = {RemoveAll}>Remove Option</button>       
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option"/>
                <button >Add Option</button>               
            </form>
        </div>
    );
    ReactDOM.render(template,appRoot); 
}
reReander();
 
