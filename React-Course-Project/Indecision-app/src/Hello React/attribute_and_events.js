/* 
    challange 
    1. make button with name -1
    2. set up minus one function 
    3. register onclike handle 
    4. show minus one when click
    5. make reset button
    6. make function that fires when clicked
    7. print reset every single click
*/


let count = 0;
const addOne = () => {
    console.log('addOne');
};


const minusOne = () => {
    console.log('minus One');
}
const reset = () => {
    console.log('reset')
}
const template = (
    <div>
        <h1>Count: {count}</h1>
        <button onClick={addOne}>+1</button>
        <button onClick={minusOne}>-1</button>
        <button onClick={reset}>Reset</button>
    </div>
)

const appRoot = document.getElementById('app');
ReactDOM.render(template, appRoot);

//jsx ar object guli print korbe
console.log(template)
