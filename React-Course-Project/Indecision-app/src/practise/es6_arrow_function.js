
//argument object is not accesible thorugh arrow function
const add = (a, b)=>{
     //console.log(arguments)
     return a + b;
}
//console.log(add(5,6))

//this key word can not bind its own value in arrow funciton

// const user = {
//     name: 'Andrew',
//     cities : ['Regina', 'Dhaka', 'Canada'],
//     printPlacesLieved: function(){
//        //console.log (this.name);
//       //console.log(this.cities);
//        //var that = this;
//         this.cities.forEach((city) => {
//             //console.log(this.name + ' has lived in ' + city);
//         });
//     }

// }

// user.printPlacesLieved();

//uses multiple arrow function

// const user = {
//     name: 'Mh-Dip',
//     cities: ['Regina','Dhaka','Canada'],
//     printPlacesLived () {
//         console.log(this.name);
//         console.log(this.cities);
//         //const that = this;

//         this.cities.forEach((city)=>{
//              console.log(this.name + 'has lived in '+ city)
//         });

//     }
// }
// user.printPlacesLived();

//using map function
// const user = {
//     name : 'ratul',
//     cities: ['Regina', 'Dhaka', 'Canada'],
//     printPlacesLived(){
//         const cityMessages = this.cities.map((city)=>{
//             return this.name + 'lived in' + ' ' + city
//         });

//         return cityMessages;
//     }
// }

// console.log(user.printPlacesLived());


//using map function with with return

const user = {
    name : 'ratul',
    cities : ['Banglades', 'Dhaka', 'new York'],
    printPlacesLived(){
        return this.cities.map((city)=>this.name + ' lived in'  + city)
    }
}

console.log(user.printPlacesLived());




