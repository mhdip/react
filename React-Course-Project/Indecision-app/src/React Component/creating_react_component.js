/*Creating React Component*/

//Creating Header
/*
    1. akhane amra global React extend korechi 
    2. global React ta hocche akta Object jeta kina page ar global property
        expose kore
    3. then amra sei object theke Component class use koreche
        jeta kina react ar sokol feather access korte help korbe 
    4. react component ar vitore always akta method declare kortei hobe
        jeta hocche render karon jsx ke manually bind korar jonno 
        render method lagbe 
*/
class Header extends React.Component {
    render (){
        return (
            <div>
                <h1>Indecision App</h1>
                <h2>How It Looks</h2>
            </div>
        );
    }
}

class Action extends React.Component {
    render(){
        return (
            <div>
                <button>What should I do?</button>
            </div>
        );
    }
}

class Option extends React.Component {
    render() {
        return (
            <div>
                <p>Options component Here</p>
            </div>    
        )  
    }
}

class AddOption extends React.Component {
    render() {
        return (
            <div>
                AddOption: <input type='text' />
            </div>    
        )  
    }
}
/* 
    Challange 
    1. option class will contain a list 
    2. some static text Option component here 
    3. Add option Component -> a text box 

*/


const jsx = (
    <div>
        <Header />
        <Action />
        <Option />
        <AddOption />
    </div>
);

ReactDOM.render(jsx, document.getElementById('app'));
