/*
    sub classes 
*/

/*
 creating new subclass
 1. travller -> person
 2. add support for homeLocation
 3. Override getGreeting
 4. if there is home lcoation -I am mh dip come form super class and then append I'm visiting from Bangladesh
 5. if there is no home location -> just staty with I am mh dip
*/
class Person {
    constructor(name = 'Anonymous', age=0){
        this.name = name;
        this.age = age;
    }

    getGreeting (){
        //regular string
       // return 'Hi, I am ' + this.name + '!';
    
       //ES6 String
       return  `Hi, I am ${this.name}!`;
    }

    getDescription(){
        return `And, I am ${this.age} ${this.age > 0 ? 'years' : 'year'} old`
    }
   
}

class Student extends Person {
    constructor(name, age, major) {
        super(name, age);
        this.major = major;
    }

    hasMajor(){
        return !!this.major;
    }

    getDescription(){
        let getDescription = super.getDescription();

        if(this.hasMajor()){
            getDescription += `. Their major is ${this.major}`
        }

        return getDescription;

        //return 'testing';

    }
}

class Traveller extends Person {
    constructor (name, age, homeLocation){
        super(name, age);
        this.homeLocation = homeLocation;
    }

    hasLocation(){
        return !!this.homeLocation;
    }

    getGreeting(){
        let getGreeting = super.getGreeting();

        if(this.hasLocation()){
            getGreeting += `. I am From ${this.homeLocation}`
        }

        return getGreeting;
        
    }

}

const me = new Traveller('MH Dip', 25, 'Bangladesh');
console.log(me.getGreeting());

const other = new Traveller();
console.log(other.getGreeting());
 

