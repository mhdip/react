
    class IndecisionApp extends React.Component {
        constructor(props){
            super(props)
            this.handleDeleteOption = this.handleDeleteOption.bind(this)
            this.handlePick = this.handlePick.bind(this)
            this.state = {
                options: ['One Thing', 'Two Thing', 'Four Thing']
            }           
        }

        handleDeleteOption(){
            this.setState(()=>{
                return {
                    options:[]
                };
            });
        }
        /*
            1.Create handlePick funciton 
            2. pass down to Action 
            3. bind to constructor  
            4. set up onClick
            5. randonly pic and option and alert it
        */ 
        handlePick(){
            const randomNum = Math.floor(Math.random()* this.state.options.length)
            const option = this.state.options[randomNum];
            alert(option)
        }
        render(){
            const title = 'Indecision';
            const subtitle = 'Pur Your life in the hands of a computer';
            
            return (
                <div>
                    <Header title={title} subtitle= {subtitle} />
                    <Action 
                        hasOption={this.state.options.length > 0}
                        handlePickOption = {this.handlePick}
                    />
                    <Options 
                        options = {this.state.options}
                        handleDeleteOption = {this.handleDeleteOption}
                    />
                    <AddOption />
                </div>
            )
        }
    }
  
    class Header extends React.Component {
        render (){
            //console.log(this.props);
            return (
                <div>
                    <h1>{this.props.title}</h1>
                    <h2>{this.props.subtitle}</h2>
                </div>
            );
        }
    }
    
    class Action extends React.Component {  
        render(){
            return (
                <div>
                    <button 
                        onClick={this.props.handlePickOption}
                        disabled={!this.props.hasOption}
                    >
                        What should i do
                    
                    </button>
                </div>
               
            );
        }
    }
    
    class Options extends React.Component {
            
        render() {
            //console.log(this.props.options)
            const optionVal = this.props.options;
            return (           
                <div>
                    <button onClick={this.props.handleDeleteOption}>Remove All</button>
                    
                        { 
                           optionVal.map((optionVal)=> <Option key={optionVal} optionText={optionVal}/>)
                            
                        }
                                
                </div>    
            )  
        }
    }
    
    
    class Option extends React.Component {
        render() { 
            return (
                <div>
                   Option: {this.props.optionText}
                </div>
            )
        }
    }
    
    class AddOption extends React.Component {
        handleAddOption(e){
            e.preventDefault();
    
            const option = e.target.elements.option.value.trim();
           
            if(option){
               
            }
        }
    
        render() {
            return (
                <div>
                    <form onSubmit={this.handleAddOption}>
                       <input type="text" name="option" />
                       <button>Add Option</button>
                    </form>
                </div>    
            )  
        }
    } 
    
    ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
    