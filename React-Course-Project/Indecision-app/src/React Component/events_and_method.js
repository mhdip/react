/*Creating React nesting Component*/


class IndecisionApp extends React.Component {
    render(){
        const title = 'Indecision';
        const subtitle = 'Pur Your life in the hands of a computer';
        const options  = ['One Thing', 'Two Thing', 'Four Thing'];

        return (
            <div>
                <Header title={title} subtitle= {subtitle} />
                <Action />
                <Options options = {options}/>
                <AddOption />
            </div>
        )
    }
}

class Header extends React.Component {
    render (){
        //console.log(this.props);
        return (
            <div>
                <h1>{this.props.title}</h1>
                <h2>{this.props.subtitle}</h2>
            </div>
        );
    }
}

class Action extends React.Component {
    handlePick(){
        alert('hi dip')
    }

    render(){
        return (
            <div>
                <button onClick={this.handlePick}>What should i do</button>
              
            </div>
           
        );
    }
}
/*
    challange 1

    1.add remove all button 
    2.setup handleRemoveAll
    3. setup onClick to fire the method


    challange 2 
    1.set up the form with text input and submit button
    2. Wire up onSubmit
    3. handleAddOption -> fetch the value typed - if value exist, then alert
*/

class Options extends React.Component {
    RemoveButton(){
        alert('remove All')
    }

    render() {
        //console.log(this.props.options)
        const optionVal = this.props.options;
        return (           
            <div>
                <button onClick={this.RemoveButton}>Remove All</button>
                
                    {
                       optionVal.map((optionVal)=> <Option key={optionVal} optionText={optionVal}/>)
                    }
                            
            </div>    
        )  
    }
}


class Option extends React.Component {
    render() { 
        return (
            <div>
               Option: {this.props.optionText}
            </div>
        )
    }
}

class AddOption extends React.Component {
    handleAddOption(e){
        e.preventDefault();

        const option = e.target.elements.option.value.trim();
       
        if(option){
           
        }
    }

    render() {
        return (
            <div>
                <form onSubmit={this.handleAddOption}>
                   <input type="text" name="option" />
                   <button>Add Option</button>
                </form>
            </div>    
        )  
    }
} 

ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
