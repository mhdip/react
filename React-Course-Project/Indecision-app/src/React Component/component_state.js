/*Creating React nesting Component*/
/*
    amra akhn object define kore data bind korbo 
*/ 
    // const obj = {
    //     name: 'MH Dip',
    //     getName : function(){
    //         return this.name;
    //     }
    // }

    // // function func(){
    // //     console.log(this)
    // // }
    // // func()

    // //amra akhane obj ar bodole ntun datao set korte pari
    // const getName = obj.getName.bind(/*obj*/ {name: 'antor'});
    // console.log(getName()) 


    class IndecisionApp extends React.Component {
        render(){
            const title = 'Indecision';
            const subtitle = 'Pur Your life in the hands of a computer';
            const options  = ['One Thing', 'Two Thing', 'Four Thing'];
    
            return (
                <div>
                    <Header title={title} subtitle= {subtitle} />
                    <Action />
                    <Options options = {options}/>
                    <AddOption />
                </div>
            )
        }
    }
    
    class Header extends React.Component {
        render (){
            //console.log(this.props);
            return (
                <div>
                    <h1>{this.props.title}</h1>
                    <h2>{this.props.subtitle}</h2>
                </div>
            );
        }
    }
    
    class Action extends React.Component {
        handlePick(){
            alert('hi dip')
        }
    
        render(){
            return (
                <div>
                    <button onClick={this.handlePick}>What should i do</button>
                   
                </div>
               
            );
        }
    }
    
    class Options extends React.Component {
        constructor(props) {
            super(props)
            this.RemoveButton = this.RemoveButton.bind(this)
        }
    
        RemoveButton(){
            //alert('remove All')
            console.log(this.props.options);
        }
    
        render() {
            //console.log(this.props.options)
            const optionVal = this.props.options;
            return (           
                <div>
                    <button onClick={this.RemoveButton}>Remove All</button>
                    
                        { 
                           optionVal.map((optionVal)=> <Option key={optionVal} optionText={optionVal}/>)
                        }
                                
                </div>    
            )  
        }
    }
    
    
    class Option extends React.Component {
        render() { 
            return (
                <div>
                   Option: {this.props.optionText}
                </div>
            )
        }
    }
    
    class AddOption extends React.Component {
        handleAddOption(e){
            e.preventDefault();
    
            const option = e.target.elements.option.value.trim();
           
            if(option){
               
            }
        }
    
        render() {
            return (
                <div>
                    <form onSubmit={this.handleAddOption}>
                       <input type="text" name="option" />
                       <button>Add Option</button>
                    </form>
                </div>    
            )  
        }
    } 
    
    ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
    