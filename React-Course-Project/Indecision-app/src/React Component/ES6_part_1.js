/*
    use Classes

    purpose of class is reuse the code

    think Car as a class 
    so 
    car has 
    --- compnay Name 
    --- Model Name 
    --- Vin

    --- something all car have same 
    getDescription  

*/

/*
    Challange to add a second constructor 
    1. Setup Constructor  to take name and age  and {default to 0}
    2. GetDescripttion - return string like {Mh Dip is 26 years old}
*/
class Person {
    constructor(name = 'Anonymous', age=0){
        this.name = name;
        this.age = age;
    }

    getGreeting (){
        //regular string
       // return 'Hi, I am ' + this.name + '!';
    
       //ES6 String
       return  `Hi, I am ${this.name}!`;
    }

    getDescription(){
        return `And, I am ${this.age} ${this.age > 0 ? 'years' : 'year'} old`
    }
   
}   

const me = new Person('MH Dip', 25);
console.log(`${me.getGreeting() + ' ' + me.getDescription()}`);

const other = new Person();
console.log(other.getGreeting() + other.getDescription());
 

