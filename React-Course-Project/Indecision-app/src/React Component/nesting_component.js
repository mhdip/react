/*Creating React nesting Component*/


class IndecisionApp extends React.Component {
    render(){
        return (
            <div>
                <Header />
                <Action />
                <Options />
                <AddOption />
            </div>
        )
    }
}

class Header extends React.Component {
    render (){
        return (
            <div>
                <h1>Indecision App</h1>
                <h2>How It Looks</h2>
            </div>
        );
    }
}

class Action extends React.Component {
    render(){
        return (
            <div>
                <button>What should I do?</button>
            </div>
        );
    }
}

class Options extends React.Component {
    render() {
        return (
            <div>
                <p>Options component Here</p>
                <Option />
            </div>    
        )  
    }
}

/*
    challange 
    1.class options ar jonno render korte hobe option

*/
class Option extends React.Component {
    render() { 
        return (
            <div>
                <p>Option Component Here </p>
            </div>
        )
    }
}

class AddOption extends React.Component {
    render() {
        return (
            <div>
                AddOption component here
            </div>    
        )  
    }
} 


ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
