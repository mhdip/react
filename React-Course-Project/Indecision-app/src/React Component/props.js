/*Creating React nesting Component*/


class IndecisionApp extends React.Component {
    render(){
        const title = 'Indecision';
        const subtitle = 'Pur Your life in the hands of a computer';
        const options  = ['One Thing', 'Two Thing', 'Four Thing'];

        return (
            <div>
                <Header title={title} subtitle= {subtitle} />
                <Action />
                <Options options = {options}/>
                <AddOption />
            </div>
        )
    }
}

class Header extends React.Component {
    render (){
        //console.log(this.props);
        return (
            <div>
                <h1>{this.props.title}</h1>
                <h2>{this.props.subtitle}</h2>
            </div>
        );
    }
}

class Action extends React.Component {
    render(){
        return (
            <div>
                <button>What should I do?</button>
            </div>
        );
    }
}

/*
    challange 
    1. Setup options prop for Options component
    2. Render the length of the array

    next challange 
    1. create a paragraph tag for each item and options array
    2. render new p tag for each option (set text, set key)


*/

class Options extends React.Component {
    render() {
        //console.log(this.props.options)
        const optionVal = this.props.options;
        return (           
            <div>
                
                    {
                       optionVal.map((optionVal)=> <Option key={optionVal} optionText={optionVal}/>)
                    }               
            </div>    
        )  
    }
}


class Option extends React.Component {
    render() { 
        return (
            <div>
               Option: {this.props.optionText}
            </div>
        )
    }
}

class AddOption extends React.Component {
    render() {
        return (
            <div>
                AddOption component here
            </div>    
        )  
    }
} 


ReactDOM.render(<IndecisionApp />, document.getElementById('app'));
