// //VisibilityToggle - render, constructor, handleToggleVisibility 
// // Visbility -> false



class VisibilityToggle extends React.Component {
    constructor(props){
        super(props);
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
        this.state = {
            visibility: false
        };
    }

    handleToggleVisibility(){
      this.setState((prevState)=> {
        return {
            visibility: !prevState.visibility
        };
      });
    }

    render(){
       return (
        <div>
            <h1>Visiblity Toggle</h1>
            <button onClick= {this.handleToggleVisibility}>
                {this.state.visibility ? 'Hide details' : 'Show details'}
            </button>
            {this.state.visibility && (
                <div>
                    <p>Hey, These are some details you can now see</p>
                </div>
            )}
        </div>
       );
    }
}

ReactDOM.render(<VisibilityToggle/>, document.getElementById('app'))



// let visibility = false;

// const  toggleVisibility = () => {
//     visibility = !visibility;
//     render();

// }

// const render = () => {
//     const jsx = (
//         <div>
//             <h1>Visibility Toggle</h1>
//             <button onClick={toggleVisibility}>
//                 {visibility ? 'Hide Details' : 'Show Details'}
//             </button>
//              {
//                  visibility && (
//                      <div>
//                         <p>Hey, These are some details you can now see</p>
//                      </div>
//                  )
//              }
//         </div>
//     )
//     ReactDOM.render(jsx, document.getElementById('app')); 
// }
// render();

// let visibility = false;

// const ToggleVisibility = () => {
//     visibility = !visibility;
//     render();
// }

// const render = () => {
//     const jsx = (
//         <div>
//             <h1>Visiblity Toggle</h1>

//             <button onClick={ToggleVisibility}>
//                {visibility ? 'Hide Details' : 'Show details'}
//             </button>
//             {
//                 visibility && (
//                     <div>
//                      <p>Hey, These are some detail syou can now see!</p>
//                     </div>
//             )}         
//         </div>
        
//     )
    
//     ReactDOM.render(jsx, document.getElementById('app'))
// }
// render();


