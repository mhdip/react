'use strict';

/*
    1. map over app.optons getting back an array of list
    2. set key and text 
    3.
*/

var app = {
    title: 'Indecision App',
    subtitle: 'Put Your life in teh hands of a computer',
    options: []
};

var onFormSubmit = function onFormSubmit(e) {
    e.preventDefault();

    var option = e.target.elements.option.value;

    if (option) {
        app.options.push(option);
        e.target.elements.option.value = '';
        reReander();
    }
};

var RemoveAll = function RemoveAll() {
    app.options = [];
    reReander();
};
var numbers = [55, 101, 1000];
var appRoot = document.getElementById('app');
function reReander() {
    var template = React.createElement(
        'div',
        null,
        React.createElement(
            'h1',
            null,
            app.title
        ),
        app.subtitle && React.createElement(
            'p',
            null,
            app.subtitle
        ),
        React.createElement(
            'p',
            null,
            app.options.length > 0 ? 'Here are your options' : 'No options'
        ),
        React.createElement(
            'ol',
            null,
            app.options.length > 0 && app.options.map(function (option, index) {
                index = index + 1;
                return React.createElement(
                    'p',
                    { key: index },
                    'Text: ',
                    option
                );
            })
        ),
        React.createElement(
            'button',
            { onClick: RemoveAll },
            'Remove Option'
        ),
        React.createElement(
            'form',
            { onSubmit: onFormSubmit },
            React.createElement('input', { type: 'text', name: 'option' }),
            React.createElement(
                'button',
                null,
                'Add Option'
            )
        )
    );
    ReactDOM.render(template, appRoot);
}
reReander();
